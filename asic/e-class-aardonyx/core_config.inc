ISA=RV32IMAC
MUL=asic
SYNTH=ASIC
VERBOSITY=0
USERTRAPS=enable
USER=enable
COREFABRIC=AXI4Lite
MULSTAGES=4
DIVSTAGES=32
PADDR=32
RESETPC=16
ARITHTRAP=disable

CAUSESIZE=6
DEBUG=enable
OPENOCD=disable

# Verilator options
COVERAGE=none
TRACE=disable
THREADS=1
VERILATESIM=fast

# DebugOptions
RTLDUMP=disable
ASSERTIONS=disable

# Trigger Setup
TRIGGERS=2

# performance counters
COUNTERS=2

# pmp
PMP=disable
PMPSIZE=4

# For Vivado Synthesis
FPGA=xc7a35ticsg324-1l
SYNTHTOP=fpga_top
JOBS=4
